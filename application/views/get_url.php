<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<title></title>
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	
	<style type="text/css">
		body{margin-left: 30%;margin-top: 5%;}
		#container{
		text-align: center;
		background-color: #3CB371;
		width: 50%;
		height: 150px;
	}
	</style>

</head>

<body>
	<div id="container">
		<h1>URL Shortener</h1>
		<section id="main">
		<?php

			$this->load->helper('form');

			echo form_open();
			echo form_label('URL to Shorten', 'url');
			echo form_input('url');
			echo form_submit('shorty','Get Shorty');
			echo form_close();

			if(isset($short_url))
			{
				echo '<a href="'.base_url().$short_url.'" target="_blank" class="shorty_url">'.base_url().$short_url.'</a>';
			}

			if(isset($error))
			{
				echo '<div class="errors">'.$error.'</div>';
			}
		?>

		</section><!-- /main -->

	</div><!--!/#container -->
</body>
</html>