-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2019 at 08:51 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `short`
--

-- --------------------------------------------------------

--
-- Table structure for table `urls`
--

CREATE TABLE IF NOT EXISTS `urls` (
`id` int(11) NOT NULL,
  `long_url` varchar(250) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `urls`
--

INSERT INTO `urls` (`id`, `long_url`) VALUES
(1, 'https://www.arch17.com/product/580?material=Fabric&size=127%2C96%2C127'),
(2, 'http://arch17.com/product/580?material=Fabric&size=127%2C96%2C127'),
(3, 'http://arch17.com/product/580?material=Fabric&size=127%2C96%2C127'),
(4, 'http://arch17.com/product/580?material=Fabric&size=127%2C96%2C127'),
(5, 'http://arch17.com/product/580?material=Fabric&size=127%2C96%2C127'),
(6, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(7, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(8, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(9, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(10, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(11, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(12, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(13, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(14, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(15, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(16, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(17, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(18, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(19, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(20, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(21, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(22, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(23, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(24, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(25, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(26, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(27, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(28, 'http://localhost/ci/welcome'),
(29, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(30, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(31, 'http://short.local'),
(32, 'http://short.local'),
(33, 'http://short.local'),
(34, 'http://short.local'),
(35, 'http://short.local'),
(36, 'http://short.local'),
(37, 'http://short.local'),
(38, 'http://short.local'),
(39, 'http://short.local'),
(40, 'http://short.local'),
(41, 'http://short.local'),
(42, 'http://short.local'),
(43, 'http://short.local'),
(44, 'http://short.local'),
(45, 'http://short.local'),
(46, 'http://short.local'),
(47, 'http://short.local'),
(48, 'http://short.local'),
(49, 'http://short.local'),
(50, 'http://short.local'),
(51, 'http://short.local'),
(52, 'http://short.local'),
(53, 'http://short.local'),
(54, 'http://short.local'),
(55, 'http://short.local'),
(56, 'http://short.local'),
(57, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(58, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(59, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(60, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(61, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(62, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(63, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(64, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(65, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(66, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(67, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(68, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(69, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(70, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(71, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(72, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(73, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(74, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(75, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(76, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(77, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(78, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(79, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(80, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(81, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(82, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(83, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(84, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(85, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(86, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(87, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(88, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(89, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(90, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(91, 'http://arch17.com/product/580?material=Fabric&size=127,96,127'),
(92, 'http://arch17.com/product/580?material=Fabric&size=127,96,127');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `urls`
--
ALTER TABLE `urls`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `urls`
--
ALTER TABLE `urls`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=93;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
